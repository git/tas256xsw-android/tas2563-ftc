/*
** =============================================================================
** Copyright (c) 2016  Texas Instruments Inc.
**
** This program is free software; you can redistribute it and/or modify it under
** the terms of the GNU General Public License as published by the Free Software
** Foundation; version 2.
**
** This program is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
** FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along with
** this program; if not, write to the Free Software Foundation, Inc., 51 Franklin
** Street, Fifth Floor, Boston, MA 02110-1301, USA.
**
** File:
**     tas2563_ftc.c
**
** Description:
**     factory test program for TAS2563 Android devices
**
** =============================================================================
*/

#include <stdio.h>
#include <stdint.h>
#include <math.h>
#include <sys/types.h>

#include "system.h"
#include "tas2563.h"        // TAS2563 Driver
#include "tas2563_ftc_lib.h"
#include "tas2563_ftc.h"    // TAS2563 Factory Test and Calibration Tool

#define PI                   3.14159

// -----------------------------------------------------------------------------
// tas2563_ftc
// -----------------------------------------------------------------------------
// Description:
//      Obtains Re, f0, Q and T_cal from the speaker. This only needs to be
//      executed once during production line test.
// -----------------------------------------------------------------------------
int tas2563_chk(double t_cal, struct TFTCConfiguration *pFTCC)
{
	int nResult = 0;
	double nDevARe, nDevADeltaT, nDevAF0, nDevAQ;
	double nDevBRe, nDevBDeltaT, nDevBF0, nDevBQ;
	uint8_t nPGID;
	uint32_t libVersion;

	libVersion = get_lib_ver();
	printf("libVersion=0x%x\r\n", libVersion);

	/* get device PGID */
//	nPGID = tas2563_get_PGID();
//	printf("PGID=0x%x\r\n", nPGID);
	/* set device PGID to FTC process */
	//tas2563_ftc_set_PGID(nPGID);

	/* Get actual Re from TAS2563 */
	nResult = get_Re_deltaT(pFTCC->nTSpkCharDevA.nPPC3_Re0,
							pFTCC->nTSpkCharDevA.nSpkReAlpha,
							&nDevARe, &nDevADeltaT);
	nResult = get_f0_Q(pFTCC->nTSpkCharDevA.nPPC3_FWarp,
						pFTCC->nPPC3_FS,
						pFTCC->nTSpkCharDevA.nPPC3_Bl,
						pFTCC->nTSpkCharDevA.nPPC3_Mms,
						pFTCC->nTSpkCharDevA.nPPC3_Re0,
						&nDevAF0, &nDevAQ);
	printf("SPK Re = %f, DeltaT=%f, F0 = %f, Q = %f\n", nDevARe, nDevADeltaT, nDevAF0, nDevAQ);

	FILE *pFile = fopen(TAS2563_CAL_TXT_NAME, "a+");

	fprintf(pFile, "F0 = %f\n\r", nDevAF0);
	fprintf(pFile, "Q = %f\n\r", nDevAQ);

	fclose(pFile);


	tas2563_ftc_release();

    return nResult;
}