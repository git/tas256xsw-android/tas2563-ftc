/*
** =============================================================================
** Copyright (c) 2016  Texas Instruments Inc.
**
** This program is free software; you can redistribute it and/or modify it under
** the terms of the GNU General Public License as published by the Free Software
** Foundation; version 2.
**
** This program is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
** FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along with
** this program; if not, write to the Free Software Foundation, Inc., 51 Franklin
** Street, Fifth Floor, Boston, MA 02110-1301, USA.
**
** File:
**     system.h
**
** Description:
**     header file for system.c
**
** =============================================================================
*/

#ifndef SYSTEM_H_
#define SYSTEM_H_

#include <stdint.h>
#include <sys/types.h>

#ifdef ANDROID
	#define AUDIO_PLAYER "tinyplay"
	#define AUDIO_MIXER "tinymix"
#else
	#define AUDIO_PLAYER "aplay"
	#define AUDIO_MIXER "amixer"
#endif

// Hypothetical System Functions
extern pid_t sys_play_wav(char * pFile, char * pMode);
extern void sys_stop_wav(pid_t nProcess);
extern void sys_delay(uint32_t delay_ms);
extern double sys_get_ambient_temp(void);
extern int sys_is_valid(char * pFile);

#endif /* SYSTEM_H_ */