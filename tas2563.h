/*
** =============================================================================
** Copyright (c) 2016  Texas Instruments Inc.
**
** This program is free software; you can redistribute it and/or modify it under
** the terms of the GNU General Public License as published by the Free Software
** Foundation; version 2.
**
** This program is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
** FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along with
** this program; if not, write to the Free Software Foundation, Inc., 51 Franklin
** Street, Fifth Floor, Boston, MA 02110-1301, USA.
**
** File:
**     tas2563.h
**
** Description:
**     header file for tas2563.c
**
** =============================================================================
*/

#ifndef TAS2563_H_
#define TAS2563_H_

#include <stdint.h>
#include "tas2563_ftc.h"

/* 0000 0000 0BBB BBBB BPPP PPPP PRRR RRRR */

#define TAS2563_REG(book, page, reg)		((((unsigned int)book * 256 * 128) + \
						 ((unsigned int)page * 128)) + reg)

#define TAS2563_BOOK_ID(reg)			((unsigned char)(reg / (256 * 128)))
#define TAS2563_PAGE_ID(reg)			((unsigned char)((reg % (256 * 128)) / 128))
#define TAS2563_BOOK_REG(reg)			((unsigned char)(reg % (256 * 128)))
#define TAS2563_PAGE_REG(reg)			((unsigned char)((reg % (256 * 128)) % 128))

#define TILOAD_IOC_MAGIC   0xE0

#define TILOAD_IOCTL_SET_CHL			_IOW(TILOAD_IOC_MAGIC, 5, int)
#define TILOAD_IOCTL_SET_CONFIG			_IOW(TILOAD_IOC_MAGIC, 6, int)
#define TILOAD_IOCTL_SET_CALIBRATION	_IOW(TILOAD_IOC_MAGIC, 7, int)

#define RESULT_PASS			0x00000000
#define RE1_FAIL_HI			0x00000001
#define RE1_FAIL_LO			0x00000010
#define RE1_CHK_MSK			0x00000011

uint8_t tas2563_get_PGID(void);
void tas2563_mixer_command(char *pCommand, int nData);
uint32_t tas2563_coeff_read(uint32_t reg);
void tas2563_coeff_write(uint32_t reg, uint32_t data);
void tas2563_save_cal(struct TFTCConfiguration *pFTCC,
	double dev_a_re, uint32_t dev_a_rms_pow, uint32_t dev_a_t_limit,
	double t_cal, uint32_t,char * pFileName);
uint32_t check_spk_bounds(struct TFTCConfiguration *pFTCC, double re1);
void tas2563_load_calibration(int nCalibration);
void tas2563_open_bin(char * pFileName);
void tas2563_close_bin(void);
void tas2563_ftc_release(void);

#endif /* TAS2563_H_ */