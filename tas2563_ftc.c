/*
** =============================================================================
** Copyright (c) 2016  Texas Instruments Inc.
**
** This program is free software; you can redistribute it and/or modify it under
** the terms of the GNU General Public License as published by the Free Software
** Foundation; version 2.
**
** This program is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
** FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along with
** this program; if not, write to the Free Software Foundation, Inc., 51 Franklin
** Street, Fifth Floor, Boston, MA 02110-1301, USA.
**
** File:
**     tas2563_ftc.c
**
** Description:
**     factory test program for TAS2563 Android devices
**
** =============================================================================
*/

#include <stdio.h>
#include <stdint.h>
#include <math.h>
#include <sys/types.h>

#include "system.h"
#include "tas2563.h"        // TAS2563 Driver
#include "tas2563_ftc_lib.h"
#include "tas2563_ftc.h"    // TAS2563 Factory Test and Calibration Tool

#define PI                   3.14159

static struct TFTCConfiguration *gpFTCC;

// -----------------------------------------------------------------------------
// tas2563_ftc
// -----------------------------------------------------------------------------
// Description:
//      Obtains Re, f0, Q and T_cal from the speaker. This only needs to be
//      executed once during production line test.
// -----------------------------------------------------------------------------
uint32_t tas2563_ftc(double t_cal, struct TFTCConfiguration *pFTCC)
{
	int nResult = 0;
	double dev_a_re = pFTCC->nTSpkCharDevA.nPPC3_Re0;   // Default Re
	uint32_t dev_a_prm_pow = 0;           // Total RMS power coefficient
	uint32_t dev_a_prm_tlimit = 0;        // Delta temperature limit coefficient
	uint8_t nPGID;
	uint32_t libVersion;
	uint32_t result = 0;
	pid_t nPlaybackProcess;

	gpFTCC = pFTCC;

	libVersion = get_lib_ver();
	printf("libVersion=0x%x\r\n", libVersion);

	/* get device PGID */
//	nPGID = tas2563_get_PGID();
//	printf("PGID=0x%x\r\n", nPGID);

	/* set device PGID to FTC process */
	//tas2563_ftc_set_PGID(nPGID);

	/* STEP 1: Play calibration signal */
#ifdef ANDROID
	tas2563_mixer_command("PRI_MI2S_RX Audio Mixer MultiMedia1", 1); //platform dependent
	tas2563_mixer_command("TAS2563 IVSENSE ENABLE", 1); //platform dependent
	nPlaybackProcess = sys_play_wav("/data/vendor/pa_cal/silence.wav", "loop");
#else
	nPlaybackProcess = sys_play_wav("/opt/data/silence.wav", "loop");
#endif

	/* STEP 2: start calibration process */
	tas2563_ftc_start();
	/* STEP 3: Wait for algorithm to converge */
	sys_delay(gpFTCC->nCalibrationTime);

	/* STEP 4: Get actual Re from TAS2563 */
	dev_a_re = get_re(gpFTCC->nTSpkCharDevA.nPPC3_Re0);

	/* STEP 5: check speaker bounds */
	nResult = check_spk_bounds(gpFTCC, dev_a_re);

	/* STEP 6: Set temperature limit to target TMAX */
	if((nResult& RE1_CHK_MSK) == RESULT_PASS){
		dev_a_prm_pow = calc_prm_pow (dev_a_re,
			gpFTCC->nTSpkCharDevA.nSpkTMax - t_cal,
			gpFTCC->nTSpkCharDevA.nPPC3_RTV,
			gpFTCC->nTSpkCharDevA.nPPC3_RTM,
			gpFTCC->nTSpkCharDevA.nPPC3_RTVA,
			gpFTCC->nTSpkCharDevA.nPPC3_SysGain);
		dev_a_prm_tlimit = calc_prm_tlimit(gpFTCC->nTSpkCharDevA.nSpkTMax - t_cal,
			gpFTCC->nTSpkCharDevA.nSpkReAlpha,
			gpFTCC->nTSpkCharDevA.nPPC3_DevNonlinPer,
			gpFTCC->nTSpkCharDevA.nPPC3_RTV,
			gpFTCC->nTSpkCharDevA.nPPC3_RTM,
			gpFTCC->nTSpkCharDevA.nPPC3_RTVA,
			gpFTCC->nTSpkCharDevA.nPPC3_PIG);
		set_re(gpFTCC->nTSpkCharDevA.nPPC3_Re0, dev_a_re, gpFTCC->nTSpkCharDevA.nSpkReAlpha);
		set_temp_cal(dev_a_prm_pow, dev_a_prm_tlimit);
	}


	tas2563_ftc_stop();

	sys_stop_wav(nPlaybackProcess);

	/* STEP 7: Save Re, and Cal Temp into a file */
	tas2563_save_cal(gpFTCC, dev_a_re, dev_a_prm_pow, dev_a_prm_tlimit, t_cal, nResult, TAS2563_CAL_TXT_NAME);
	/* STEP 8: Save .bin file for TAS2555 driver */
	if ((nResult & RE1_CHK_MSK) == RESULT_PASS) {
		tas2563_open_bin(TAS2563_CAL_BIN_NAME);
		set_re(gpFTCC->nTSpkCharDevA.nPPC3_Re0, dev_a_re, gpFTCC->nTSpkCharDevA.nSpkReAlpha);
		set_temp_cal(dev_a_prm_pow, dev_a_prm_tlimit);

		tas2563_close_bin();
	}
	if (gpFTCC->bLoadCalibration)
		tas2563_load_calibration(0xFF);


	tas2563_ftc_release();

	return result;
}