CROSS_PATH = /home/ju/ti-processor-sdk-linux-am335x-evm-05.00.00.15/linux-devkit/sysroots/x86_64-arago-linux/usr/bin/arm-linux-gnueabihf-
fct: tas2563.c  tas2563_chk.c tas2563_ftc.c tas2563.c system.c factorytest.c
	make -C ./lib
	$(CROSS_PATH)gcc -shared -fPIC -static -c tas2563.c -lm
	$(CROSS_PATH)gcc -shared -fPIC -static -c tas2563_chk.c -lm
	$(CROSS_PATH)gcc -shared -fPIC -static -c tas2563_ftc.c -lm ${CFLAGS}
	$(CROSS_PATH)gcc -shared -fPIC -static -c system.c -lm
	$(CROSS_PATH)gcc -shared -fPIC -static -c factorytest.c -lm
	$(CROSS_PATH)gcc -fPIC -o factorytest factorytest.o system.o tas2563_ftc.o tas2563_chk.o  tas2563.o -L./lib -lftc32 -lm ${CFLAGS}