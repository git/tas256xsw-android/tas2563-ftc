/*
** =============================================================================
** Copyright (c) 2016  Texas Instruments Inc.
**
** This program is free software; you can redistribute it and/or modify it under
** the terms of the GNU General Public License as published by the Free Software
** Foundation; version 2.
**
** This program is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
** FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along with
** this program; if not, write to the Free Software Foundation, Inc., 51 Franklin
** Street, Fifth Floor, Boston, MA 02110-1301, USA.
**
** File:
**     system.c
**
** Description:
**     system functions for TAS2555 factory test program
**
** =============================================================================
*/

#include "system.h"

#include <sys/wait.h>
#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

pid_t sys_play_wav(char * pFile, char * pMode)
{
	pid_t nProcess;
	int nStatus, nTimeout;
	char *pArgs[] = {AUDIO_PLAYER, pFile, NULL};
	char *pEnv[] = {NULL};

	if (0 == (nProcess = fork()))
	{
		if (execve(AUDIO_PLAYER, pArgs, pEnv) == -1)
		{
			printf("factorytest: Can't play %s. Please install %s. \n\r", pFile, AUDIO_PLAYER);
			exit(-1);
		}
	}

	sys_delay(500);

	printf("factorytest: Started playback of %s\n\r", pFile);

	return nProcess;
}

extern void sys_stop_wav(pid_t nProcess)
{
	char *pEnv[] = {NULL};

	printf("factorytest: Stop playback.\n\r");

	kill(nProcess, SIGKILL);
}

extern void sys_delay(uint32_t delay_ms)
{
	usleep(delay_ms * 1000);
}

extern double sys_get_ambient_temp(void)
{
	return 0.0;
}

extern int sys_is_valid(char * pFile)
{
	return 0;
}