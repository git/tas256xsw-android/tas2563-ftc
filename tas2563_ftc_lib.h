/*
** =============================================================================
** Copyright (c) 2016  Texas Instruments Inc.
**
** This program is free software; you can redistribute it and/or modify it under
** the terms of the GNU General Public License as published by the Free Software
** Foundation; version 2.
**
** This program is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
** FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along with
** this program; if not, write to the Free Software Foundation, Inc., 51 Franklin
** Street, Fifth Floor, Boston, MA 02110-1301, USA.
**
** File:
**     tas2563_lib_ftc.h
**
** Description:
**     header file for tas2563_lib_ftc.c
**
** =============================================================================
*/

#ifndef TAS2563_LIB_FTC_H_
#define TAS2563_LIB_FTC_H_

#include <stdint.h>
#include <stdbool.h>

#include "tas2563.h"

int get_lib_ver(void);
void tas2563_ftc_set_PGID(unsigned char nPGID);
void tas2563_ftc_start(void);
void set_re(double re_ppc3, double re, double alpha);
void set_temp_cal(uint32_t prm_pow, uint32_t prm_tlimit);
double get_re(double re_ppc3);
double CalcRe(double re_ppc3, uint32_t prm_r0);
uint32_t calc_prm_pow(double re, double delta_t_max, double nRTV, double nRTM, double nRTVA, double nSysGain);
uint32_t calc_prm_tlimit(double delta_t_max, double alpha, double nDevNonlinPer, double nRTV, double nRTM, double nRTVA, double nPIG);
void tas2563_ftc_stop(void);

/* below functions are used in SPK measurement only */
int get_Re_deltaT(double nPPC3_Re, double nPPC3_alpha, double *pnRe, double *pnDeltaT);
int get_f0_Q(double nPPC3_FWarp, double nPPC3_nFS, double nPPC3_Bl, double nPPC3_Mms, double nPPC3_Re0, double *pnF0, double *pnQ);

#endif /* TAS2563_LIB_FTC_H_ */