/*
** =============================================================================
** Copyright (c) 2016  Texas Instruments Inc.
**
** This program is free software; you can redistribute it and/or modify it under
** the terms of the GNU General Public License as published by the Free Software
** Foundation; version 2.
**
** This program is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
** FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along with
** this program; if not, write to the Free Software Foundation, Inc., 51 Franklin
** Street, Fifth Floor, Boston, MA 02110-1301, USA.
**
** File:
**     tas2563_ftc.h
**
** Description:
**     header file for tas2563_ftc.c
**
** =============================================================================
*/

#ifndef TAS2563_FTC_H_
#define TAS2563_FTC_H_

#include <stdint.h>
#include <stdbool.h>

struct TSPKCharData {
	double nSpkTMax;
	double nSpkReTolPer;
	double nSpkReAlpha;

	double nPPC3_Re0;
	double nPPC3_FWarp;
	double nPPC3_Bl;
	double nPPC3_Mms;
	double nPPC3_RTV;
	double nPPC3_RTM;
	double nPPC3_RTVA;
	double nPPC3_SysGain;
	double nPPC3_DevNonlinPer;
	double nPPC3_PIG;

	double nReHi;
	double nReLo;
};

struct TFTCConfiguration {
	bool bVerbose;
	bool bLoadCalibration;
	unsigned int nCalibrationTime;
	double nPPC3_FS;

	struct TSPKCharData nTSpkCharDevA;
};

#ifdef ANDROID
	#define TAS2563_CAL_TXT_NAME    "/mnt/vendor/persist/audio/tas2563_cal.txt"
	#define TAS2563_CAL_BIN_NAME    "/mnt/vendor/persist/audio/tas2563_cal.bin"
#else
	#define TAS2563_CAL_TXT_NAME    "tas2563_cal.txt"
	#define TAS2563_CAL_BIN_NAME    "tas2563_cal.bin"
#endif

uint32_t tas2563_ftc(double t_cal, struct TFTCConfiguration *pFTCC);
int tas2563_chk(double t_cal, struct TFTCConfiguration *pFTCC);
#endif /* TAS2563_FTC_H_ */